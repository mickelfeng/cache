/**
 * @Author: aesoper
 * @Description:
 * @File:  ssdb_test
 * @Version: 1.0.0
 * @Date: 2020/6/4 23:24
 */

package ssdb

import (
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestNewCache(t *testing.T) {
	client := NewCache()
	err :=client.StartAndConfigure()
	assert.Nil(t, err)

	err = client.Put("test", "test", time.Minute)
	assert.Nil(t, err)

	res, err := client.Get("test")
	assert.Nil(t, err)
	assert.Equal(t, "test", string(res))

	err = client.Delete("test")
	assert.Nil(t, err)
}
