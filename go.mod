module gitee.com/aesoper/cache

go 1.14

require (
	github.com/bradfitz/gomemcache v0.0.0-20190913173617-a41fca850d0b
	github.com/go-redis/redis/v8 v8.0.0-beta.1
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/seefan/gossdb v1.1.2
	github.com/stretchr/testify v1.5.1
)
