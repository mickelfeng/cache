/**
 * @Author: aesoper
 * @Description:
 * @File:  memory_test
 * @Version: 1.0.0
 * @Date: 2020/6/4 19:59
 */

package memory

import (
	"bytes"
	"encoding/gob"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestNewCache(t *testing.T) {
	cache := NewCache()
	err := cache.StartAndConfigure()
	assert.Nil(t, err)

	err = cache.Put("test", "test", time.Minute)
	assert.Nil(t, err)

	res, err := cache.Get("test")
	assert.Nil(t, err)
	s := ""
	gob.NewDecoder(bytes.NewReader(res)).Decode(&s)
	assert.Equal(t, "test", s)
	//fmt.Println(string(res))


	err = cache.Put("test11", map[string]interface{}{"test": "test"}, time.Minute)
	assert.Nil(t, err)

	res1, err := cache.Get("test11")
	assert.Nil(t, err)

	testmapData :=make(map[string]interface{})
	gob.NewDecoder(bytes.NewReader(res1)).Decode(&testmapData)

	v, _ :=  testmapData["test"]
	assert.Equal(t, "test", v)
}
